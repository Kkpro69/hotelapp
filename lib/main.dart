import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:hotel_app_2/routes.dart';
import 'package:hotel_app_2/screen/login/login_screen.dart';
import 'package:hotel_app_2/theme.dart';

void main() async {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Froton Hotel Sragen",
      home: AnimatedSplashScreen(
          splash: Image.asset(
            'assets/images/7.png',
            height: 100,
            width: 150,
          ),
          duration: 3000,
          splashTransition: SplashTransition.scaleTransition,
          nextScreen: Tampilan()),
    ),
  );
}

class Tampilan extends StatelessWidget {
  const Tampilan({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: theme(),
      initialRoute: LoginScreen.routeName,
      routes: routes,
    );
  }
}
