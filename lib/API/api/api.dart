import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../utils/constants.dart';
import '../models/Sliderbanner.dart';

class Api {
  // Banner
  Future<List<SliderBanner>> getToApiSlider() async {
    String bannerUrl = Constants.BannerUrl;
    final response = await http.get(Uri.parse(bannerUrl));

    if (response.statusCode == 200) {
      final decodedData = json.decode(response.body);
      final dataResult = decodedData['result'];

      if (dataResult.containsKey('data') && dataResult['data'] is List) {
        return (dataResult['data'] as List)
            .map((banner) => SliderBanner.fromJson(banner))
            .toList();
      } else {
        throw Exception(
            'Invalid response format: "results" key not found or not a List');
      }
    } else {
      throw Exception('Something happened');
    }
  }

  //menu
  // Future<List<OrderMenu>> getToApiOrder() async {
  //   String orderUrl = Constants.OrderUrl;
  //   final response = await http.get(Uri.parse(Constants.OrderUrl));
  //   if (response.statusCode == 200) {
  //     final decodedData = json.decode(response.body);
  //     final dataResult = decodedData['result'];

  //     if (dataResult.containsKey('data') && dataResult['data'] is List) {
  //       return (dataResult['data'] as List)
  //           .map((order) => OrderMenu.fromJson(order))
  //           .toList();
  //     } else {
  //       throw Exception(
  //           'Invalid response format: "results" key not found or not a List');
  //     }
  //   } else {
  //     print('HTTP Status Code: ${response.statusCode}');
  //     throw Exception(
  //         'HTTP request failed with status code ${response.statusCode}');
  //   }
  // }
  Future<List<OrderMenu>> getToApiOrder() async {
    try {
      String orderUrl = Constants.OrderUrl;
      final headers = {
        'Authorization':
            'https://frontone-api.hepytech.com/api/category-menu/show?sortby=asc&orderby=id&per_page=5&page=1', // Replace with your actual API key or token
      };

      final response = await http.get(
        Uri.parse(orderUrl),
        headers: headers,
      );

      if (response.statusCode == 200) {
        final decodedData = json.decode(response.body);
        final dataResult = decodedData['result'];

        if (dataResult.containsKey('data') && dataResult['data'] is List) {
          return (dataResult['data'] as List)
              .map((order) => OrderMenu.fromJson(order))
              .toList();
        } else {
          throw Exception(
              'Invalid response format: "results" key not found or not a List');
        }
      } else {
        print('HTTP Status Code: ${response.statusCode}');
        throw Exception(
            'HTTP request failed with status code ${response.statusCode}');
      }
    } catch (error) {
      print('Error: $error');
      throw Exception('An error occurred while making the request.');
    }
  }
}


  // // Level
  // Future<List<Level>> getToApiLevel() async {
  //   String levelURl = Constants.Level;
  //   final response = await http.get(Uri.parse(levelURl));

  //   if (response.statusCode == 200) {
  //     final decodedData = json.decode(response.body);
  //     final dataResult = decodedData['result'];

  //     if (dataResult.containsKey('data') && dataResult['data'] is List) {
  //       return (dataResult['data'] as List)
  //           .map((level) => Level.fromJson(level))
  //           .toList();
  //     } else {
  //       throw Exception(
  //           'Invalid response format: "results" key not found or not a List');
  //     }
  //   } else {
  //     throw Exception('Something happened');
  //   }
  // }

