class SliderBanner {
  int id;
  String title;
  String image;

  SliderBanner({required this.image, required this.id, required this.title});

  factory SliderBanner.fromJson(Map<String, dynamic> json) {
    return SliderBanner(
        image: json['image'], id: json['id'], title: json['title']);
  }
}

class loginUser {
  int id;
  int phone;
  String username;
  String email;
  String id_Member;
  String token_type;
  String access_token;

  loginUser({
    required this.id,
    required this.phone,
    required this.email,
    required this.id_Member,
    required this.username,
    required this.access_token,
    required this.token_type,
  });
  factory loginUser.fromJson(Map<String, dynamic> json) {
    return loginUser(
        id: json['id'],
        phone: json['phone'],
        email: json['email'],
        id_Member: json['id_member'],
        username: json['username'],
        access_token: json['access_token'],
        token_type: json['token_type']);
  }
}

class Regis {
  String name;
  int phone;
  String username;
  String email;
  String password;
  String confirm_password;
  String tanggal_lahir;

  Regis({
    required this.name,
    required this.phone,
    required this.username,
    required this.email,
    required this.password,
    required this.confirm_password,
    required this.tanggal_lahir,
  });

  factory Regis.fromJson(Map<String, dynamic> json) {
    return Regis(
        name: json['name'],
        phone: json['phone'],
        username: json['username'],
        email: json['email'],
        password: json['password'],
        confirm_password: json['confirm_password'],
        tanggal_lahir: json['tanggal_lahir']);
  }
}

class Level {
  int id;
  String title;
  String img;
  String desc;

  Level(
      {required this.img,
      required this.id,
      required this.title,
      required this.desc});

  factory Level.fromJson(Map<String, dynamic> json) {
    return Level(
      img: json['image'],
      id: json['id'],
      title: json['title'],
      desc: json['desc'],
    );
  }
}

class OrderMenu {
  int id;
  String name;

  OrderMenu({required this.id, required this.name});

  factory OrderMenu.fromJson(Map<String, dynamic> json) {
    return OrderMenu(
      id: json['id'],
      name: json['name'],
    );
  }
}
