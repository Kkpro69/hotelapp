import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/API/api/api.dart';
import 'package:hotel_app_2/API/models/Sliderbanner.dart';
import 'package:hotel_app_2/menu%20screen/bannerSlider.dart';
import 'package:hotel_app_2/screen/card.dart';
import 'package:hotel_app_2/screen/level.dart';
import 'package:hotel_app_2/screen/menu_dasbord.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Future<List<SliderBanner>> ImageBanner;

  @override
  void initState() {
    super.initState();
    ImageBanner = Api().getToApiSlider();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white70,
        elevation: 0,
        title: Text(
          'Froton Hotel Sragen ',
          style: GoogleFonts.ptSans(
              color: Colors.amber, fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.qr_code),
            onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                icon: Icon(
                  Icons.qr_code,
                  size: 150,
                ),
              ),
            ),
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {},
            color: Colors.black,
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 10),
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              child: FutureBuilder(
                future: ImageBanner,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(snapshot.error.toString()),
                    );
                  } else if (snapshot.hasData) {
                    final data = snapshot.data;
                    return BannerSlider(
                      snapshot: snapshot,
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(left: 10),
              child: Container(
                height: 30,
                width: 190,
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(30)),
                child: Text(
                  'Your Elite Level',
                  style: GoogleFonts.ptSans(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              child: cardQR(),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              child: LevelCard(),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              child: Column(
                children: [
                  CardMenu1(),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  CardMenu2(),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  CardMenu3(),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  CardMenu4(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
