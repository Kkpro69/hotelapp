import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/menu%20screen/more_screen.dart';
import 'package:hotel_app_2/menu%20screen/myvoucher_screen.dart';
import 'package:hotel_app_2/menu%20screen/order_screen.dart';
import 'package:hotel_app_2/menu%20screen/poin_history.dart';
import 'package:hotel_app_2/menu%20screen/privilage_screen.dart';
import 'package:hotel_app_2/menu%20screen/reedem_screen.dart';
import 'package:hotel_app_2/menu%20screen/stay_screen.dart';
import 'package:hotel_app_2/menu%20screen/transaction_history_screen.dart';

class CardMenu1 extends StatelessWidget {
  const CardMenu1({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          ClipRRect(
            child: Container(
              color: Colors.transparent,
              height: 110.0,
              width: 428.0,
            ),
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 20.0,
            child: Container(
              decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 15,
                    offset: Offset(3, 8),
                    spreadRadius: -5,
                    color: Color.fromARGB(0, 6, 6, 6),
                  ),
                ],
              ),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                color: Colors.white,
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                        builder: (BuildContext context) => MyVoucherScreen(),
                      ),
                    );
                  },
                  child: SizedBox(
                    width: 184,
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(top: 5)),
                        Container(
                          child: Icon(
                            Icons.style,
                            size: 50,
                          ),
                        ),
                        const Padding(
                            padding: EdgeInsets.only(
                          top: 10,
                        )),
                        Container(
                          child: Text(
                            'My Voucher',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            right: 20.0,
            top: 0.0,
            bottom: 0.0,
            child: Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15,
                    offset: Offset(3, 8),
                    spreadRadius: -5,
                    color: Color.fromARGB(0, 0, 0, 1))
              ]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                color: Colors.white,
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            TransactionHistory()));
                  },
                  child: SizedBox(
                    width: 184,
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Icon(
                            Icons.payments,
                            size: 50,
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Text(
                            'Transaction History',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CardMenu2 extends StatelessWidget {
  const CardMenu2({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          ClipRRect(
            child: Container(
              color: Colors.transparent,
              height: 110.0,
              width: 428.0,
            ),
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 20.0,
            child: Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15,
                    offset: Offset(3, 8),
                    spreadRadius: -5,
                    color: Color.fromARGB(0, 6, 6, 6))
              ]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                color: Colors.white,
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (BuildContext context) => PoinHistory()));
                  },
                  child: SizedBox(
                    width: 184,
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Icon(
                            Icons.storage,
                            size: 50,
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Text(
                            'Poin History',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            right: 20.0,
            top: 0.0,
            bottom: 0.0,
            child: Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15,
                    offset: Offset(3, 8),
                    spreadRadius: -5,
                    color: Color.fromARGB(0, 6, 6, 6))
              ]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                color: Colors.white,
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (BuildContext context) => ReedemScreen()));
                  },
                  child: SizedBox(
                    width: 184,
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Icon(
                            Icons.redeem_rounded,
                            size: 50,
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Text(
                            'Reedem',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CardMenu3 extends StatelessWidget {
  const CardMenu3({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          ClipRRect(
            child: Container(
              color: Colors.transparent,
              height: 110.0,
              width: 428.0,
            ),
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 20.0,
            child: Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15,
                    offset: Offset(3, 8),
                    spreadRadius: -5,
                    color: Color.fromARGB(0, 6, 6, 6))
              ]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                color: Colors.white,
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (BuildContext context) => OrderScreen()));
                  },
                  child: SizedBox(
                    width: 184,
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Icon(
                            Icons.bookmark_add,
                            size: 50,
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Text(
                            'Order',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            right: 20.0,
            top: 0.0,
            bottom: 0.0,
            child: Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15,
                    offset: Offset(3, 8),
                    spreadRadius: -5,
                    color: Color.fromARGB(0, 6, 6, 6))
              ]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                color: Colors.white,
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (BuildContext context) => StayScreen()));
                  },
                  child: SizedBox(
                    width: 184,
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Icon(
                            Icons.hotel,
                            size: 50,
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Text(
                            'Stay',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CardMenu4 extends StatelessWidget {
  const CardMenu4({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          ClipRRect(
            child: Container(
              color: Colors.transparent,
              height: 110.0,
              width: 428.0,
            ),
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 20.0,
            child: Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15,
                    offset: Offset(3, 8),
                    spreadRadius: -5,
                    color: Color.fromARGB(0, 6, 6, 6))
              ]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                color: Colors.white,
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (BuildContext context) => PrivilageScreen()));
                  },
                  child: SizedBox(
                    width: 184,
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Icon(
                            Icons.all_inbox,
                            size: 50,
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Text(
                            'Privilage',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            right: 20.0,
            top: 0.0,
            bottom: 0.0,
            child: Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15,
                    offset: Offset(3, 8),
                    spreadRadius: -5,
                    color: Color.fromARGB(0, 6, 6, 6))
              ]),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                color: Colors.white,
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (BuildContext context) => MoreScreen()));
                  },
                  child: SizedBox(
                    width: 184,
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Icon(
                            Icons.more_horiz,
                            size: 50,
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Container(
                          child: Text(
                            'More',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
