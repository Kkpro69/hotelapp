import 'package:flutter/material.dart';

import '../../components/Register/RegisterComponents.dart';
import '../../size_config.dart';

class RegisterScreen extends StatelessWidget {
  static String routeName = "/register";

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 10),
        physics: const BouncingScrollPhysics(),
        child: RegisComponent()),
    );
  }
}
