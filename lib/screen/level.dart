import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../utils/constants.dart';
import 'package:http/http.dart' as http;

class LevelCard extends StatelessWidget {
  const LevelCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Container(
              color: Colors.white70,
              height: 150.0,
              width: 400.0,
            ),
          ),
          Positioned(
            left: 30.0,
            top: 30.0,
            child: Column(
              children: [
                Container(
                  width: 60,
                  height: 60,
                  decoration: ShapeDecoration(
                    color: Color.fromARGB(192, 192, 192, 192),
                    shape: CircleBorder(),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 10)),
                Container(
                  child: Title(
                      color: Colors.white,
                      child: Column(
                        children: [
                          Container(
                            child: Text('level'),
                          ),
                          Container(
                            child: Text(
                              'Total Poin 0',
                              style: GoogleFonts.ptSans(
                                  color: Colors.black,
                                  fontSize: 10,
                                  fontWeight: FontWeight.normal),
                            ),
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
          Positioned(
            left: 160.0,
            top: 30.0,
            child: Column(
              children: [
                Container(
                  width: 60,
                  height: 60,
                  decoration: const ShapeDecoration(
                    color: Color.fromARGB(255, 255, 215, 0),
                    shape: CircleBorder(),
                  ),
                ),
                const Padding(padding: EdgeInsets.only(top: 10)),
                Container(
                  child: Title(
                      color: Colors.white,
                      child: Column(
                        children: [
                          Container(
                            child: Text(
                              'GOLD',
                              style: GoogleFonts.ptSans(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            child: Text(
                              'Total Poin 1000',
                              style: GoogleFonts.ptSans(
                                  color: Colors.black,
                                  fontSize: 10,
                                  fontWeight: FontWeight.normal),
                            ),
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
          Positioned(
            right: 30.0,
            top: 30.0,
            child: Column(
              children: [
                Container(
                  width: 60,
                  height: 60,
                  decoration: const ShapeDecoration(
                    color: Color.fromARGB(229, 229, 228, 226),
                    shape: CircleBorder(),
                  ),
                ),
                const Padding(padding: EdgeInsets.only(top: 10)),
                Container(
                  child: Title(
                      color: Colors.white,
                      child: Column(
                        children: [
                          Container(
                            child: Text(
                              'PLATINUM',
                              style: GoogleFonts.ptSans(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            child: Text(
                              'Total Poin 12000',
                              style: GoogleFonts.ptSans(
                                  color: Colors.black,
                                  fontSize: 10,
                                  fontWeight: FontWeight.normal),
                            ),
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  // Future<Map<String, dynamic>> LevelApi() async {
  //   final response = await http.get(Uri.parse(Constants.LevelUrl));

  //   if (response.statusCode == 200) {
  //     return json.decode(response.body);
  //   } else {
  //     throw Exception('Failed to load level data');
  //   }
  // }
}





// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:flutter/material.dart';

// class LevelCard extends StatelessWidget {
//   const LevelCard({
//     super.key,
//     required this.snapshot,
//   });

//   final AsyncSnapshot snapshot;

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: double.infinity,
//       child: CarouselSlider.builder(
//         itemCount: snapshot.data!.length,
//         options: CarouselOptions(
//             height: 170,
//             autoPlay: true,
//             viewportFraction: 0.55,
//             enlargeCenterPage: true,
//             pageSnapping: true,
//             autoPlayCurve: Curves.fastOutSlowIn,
//             autoPlayAnimationDuration: const Duration(seconds: 1)),
//         itemBuilder: (context, itemIndex, pageViewIndex) {
//           return GestureDetector(
//             onTap: () {},
//             child: ClipRRect(
//               borderRadius: BorderRadius.circular(12),
//               child: SizedBox(
//                 height: 200,
//                 child: Image.network(
//                   (snapshot.data[itemIndex]).image,
//                   filterQuality: FilterQuality.high,
//                   fit: BoxFit.cover,
//                 ),
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   }
// }