import 'package:flutter/material.dart';
import 'package:hotel_app_2/screen/home_screen.dart';

import '../../size_config.dart';

class Home_Screen extends StatelessWidget{

  static String routeName = "/Home_screen";

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
   return const Scaffold(
    body:HomeScreen(),
    
    
   );
  }

}