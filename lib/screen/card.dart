import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class cardQR extends StatelessWidget {
  const cardQR({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Container(
              color: Color.fromARGB(255, 165, 165, 165),
              height: 150.0,
              width: 380.0,
            ),
          ),
          Positioned(
            left: 50.0,
            top: 27.0,
            child: Container(
              child: Text(
                '1000',
                style: GoogleFonts.ptSans(
                    color: Colors.white,
                    fontSize: 35,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Positioned(
            right: 30.0,
            top: 30.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Container(
                color: Colors.white,
                height: 80.0,
                width: 80.0,
                child: TextButton(
                  onPressed: () => showDialog(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      icon: Icon(
                        Icons.qr_code,
                        size: 150,
                      ),
                    ),
                  ),
                  child: Icon(
                    Icons.qr_code,
                    size: 50,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            left: 50.0,
            top: 70.0,
            child: Container(
              child: Text(
                'Silver',
                style: GoogleFonts.ptSans(
                    color: Colors.white,
                    fontSize: 28,
                    fontWeight: FontWeight.normal),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
