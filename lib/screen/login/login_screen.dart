import 'package:flutter/material.dart';
import 'package:hotel_app_2/components/login/LoginComponents.dart';

import '../../size_config.dart';

class LoginScreen extends StatelessWidget {
  static String routeName = "/sign_in";

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
      ),
      body: LoginComponents(),
    );
  }
}
