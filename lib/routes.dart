
import 'package:flutter/material.dart';
import 'package:hotel_app_2/menu%20screen/deskripsi_wedding.dart';
import 'package:hotel_app_2/screen/home/home_screen.dart';
import 'package:hotel_app_2/screen/login/login_screen.dart';
import 'package:hotel_app_2/screen/register/RegisterScreen.dart';

final Map<String, WidgetBuilder> routes ={
  LoginScreen.routeName: (context) => LoginScreen(),
  RegisterScreen.routeName: (context) => RegisterScreen(),
  Home_Screen.routeName:(context) => Home_Screen(),
  // DeskripsiWedding.routeName:(context) => DeskripsiWedding(),

};