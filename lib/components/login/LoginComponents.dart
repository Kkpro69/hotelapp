import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/utils/constants.dart';
import 'package:simple_shadow/simple_shadow.dart';

import '../../size_config.dart';
import 'Login_Form.dart';

class LoginComponents extends StatelessWidget {
  const LoginComponents({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SizedBox(
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenHeight(20)),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.04,
                    ),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.04,
                    ),
                    SimpleShadow(
                      // ignore: sort_child_properties_last
                      child: Image.asset(
                        'assets/images/7.png',
                        height: 150,
                        width: 202,
                      ),
                      opacity: 0.5,
                      color: kSecondaryColor,
                      offset: const Offset(5, 5),
                      sigma: 2,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Login!',
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xFF23374D),
                                  fontSize: 14),
                            )
                          ],
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    SignInForm()
                  ],
                ),
              ),
            )));
  }
}
