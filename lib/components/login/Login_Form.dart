import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hotel_app_2/API/models/Sliderbanner.dart';
import 'package:hotel_app_2/screen/home/home_screen.dart';
import 'package:http/http.dart' as http;
import '../../utils/constants.dart';
import '../../screen/register/RegisterScreen.dart';
import '../../size_config.dart';
import '../custom_surfix_icon.dart';
import '../default_bottom_costume_color.dart';

class SignInForm extends StatefulWidget {
  @override
  _SignInForm createState() => _SignInForm();
}

class _SignInForm extends State<SignInForm> {
  final _formKey = GlobalKey<FormState>();
  String? email;
  String? Password;
  bool? remember = false;

  TextEditingController txtEmail = TextEditingController(),
      txtPassword = TextEditingController();

  FocusNode focusNode = new FocusNode();

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          buildEmail(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPassword(),
          SizedBox(height: getProportionateScreenHeight(30)),
          Row(
            children: [
              Checkbox(
                  value: remember,
                  onChanged: (value) {
                    setState(() {
                      remember = value;
                    });
                  }),
              Text('Tetap Masuk'),
              Spacer(),
              GestureDetector(
                onTap: () {},
                child: const Text(
                  'Lupa Password',
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          DefaultButtonCustomeColor(
            color: kColorYellow,
            text: 'MASUK',
            press: () {
              prosesLogin(txtEmail.text, txtPassword.text);
            },
          ),
          SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, RegisterScreen.routeName);
            },
            child: const Text(
              'Belum Punya Akun? Daftar Disini',
              style: TextStyle(decoration: TextDecoration.underline),
            ),
          )
        ],
      ),
    );
  }

  TextFormField buildEmail() {
    return TextFormField(
      controller: txtEmail,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
        labelText: 'Email',
        hintText: 'Masukan Email anda',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: const CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }

  TextFormField buildPassword() {
    return TextFormField(
      controller: txtPassword,
      obscureText: true,
      style: mTitleStyle,
      decoration: InputDecoration(
        labelText: 'Password',
        hintText: 'Masukan Password anda',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: const CustomSurffixIcon(svgIcon: 'assets/icons/Lock.svg'),
      ),
    );
  }

  prosesLogin(email, Password) async {
    try {
      var response = await http.post(Uri.parse(Constants.LoginUrl), body: {
        'email': email,
        'password': Password,
      });
      if (response.statusCode == 200) {
        final decodedData = json.decode(response.body);
        final accessToken = decodedData['result']['access_token'];

        if (accessToken != null) {
          print('Token: ${accessToken}');
          print('Berhasil Login');
          Navigator.pushNamed(context, Home_Screen.routeName);
        } else {
          throw Exception('Token not found in response');
        }
      } else {
        print('Akun Tidak Terdaftar');
      }
    } catch (e) {
      print('Error: $e');
    }
  }
  // if (response.statusCode == 200) {
  //   var data = json.decode(response.body);
  //   print(data['access_token'].toString());
  //   print('berhasil login');
  // } else {
  //   print('failed');
  // }

  // void prosesLogin(email, password) async {
  //   var response = await http.post(Uri.parse(Constants.LoginUrl));
  //   utilApps.showDialog(context);

  //   bool status;
  //   var msg;
  //   var dataUser;

  //   try {
  //     response = await http.post(
  //       Uri.parse(
  //         Constants.LoginUrl,
  //         data: {
  //           'email': email,
  //           'password': password,
  //         },
  //       ),
  //     );
  //     status = response!.data['sukses'];
  //     msg = response!.data['msg'];

  //     if (status) {
  //       // ignore: use_build_context_synchronously
  //       AwesomeDialog(
  //         context: context,
  //         dialogType: DialogType.success,
  //         animType: AnimType.rightSlide,
  //         desc: 'Berhasil Login',
  //         btnOkOnPress: () {
  //           utilApps.hideDialog(context);
  //           dataUser = response!.data['data'];
  //           if (dataUser['role'] == 1) {
  //             Navigator.pushNamed(context, Home_Screen.routeName);
  //           } else if (dataUser['role'] == 2) {
  //             print("lempar ke halaman admin");
  //           } else {
  //             print("Halaman tidak tersedia ");
  //           }
  //           // Navigator.pushReplacementNamed(context, LoginScreen.routeName);
  //         },
  //       ).show();
  //     } else {
  //       // ignore: use_build_context_synchronously
  //       AwesomeDialog(
  //         context: context,
  //         dialogType: DialogType.error,
  //         animType: AnimType.rightSlide,
  //         title: 'Peringatan',
  //         desc: 'Gagal Login $msg',
  //         btnOkOnPress: () {
  //           utilApps.hideDialog(context);
  //         },
  //       ).show();
  //     }
  //   } catch (e) {
  //     // ignore: use_build_context_synchronously
  //     AwesomeDialog(
  //       context: context,
  //       dialogType: DialogType.error,
  //       animType: AnimType.rightSlide,
  //       title: 'Peringatan',
  //       desc: 'Terjadi Kesalahan Pada Server',
  //       btnOkOnPress: () {
  //         utilApps.hideDialog(context);
  //       },
  //     ).show();
  //   }
  // }
}
