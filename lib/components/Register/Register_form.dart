import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hotel_app_2/screen/login/login_screen.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import '../../size_config.dart';
import '../../utils/constants.dart';
import '../custom_surfix_icon.dart';
import '../default_bottom_costume_color.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterForm createState() => _RegisterForm();
}

class _RegisterForm extends State<RegisterForm> {
  String? email;
  String? Password;

  TextEditingController txtUserName = TextEditingController(),
      txtPhoneNomber = TextEditingController(),
      txtNamalengkap = TextEditingController(),
      txtPassword = TextEditingController(),
      txtEmail = TextEditingController(),
      txtDateBirthDay = TextEditingController(),
      txtConfirmPass = TextEditingController();

  FocusNode focusNode = new FocusNode();

  Response? response;
  final dio = Dio();

  @override
  void initState() {
    txtDateBirthDay.text = "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          Namalengkap(),
          SizedBox(height: getProportionateScreenHeight(30)),
          PhoneNomber(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildUserName(),
          SizedBox(height: getProportionateScreenHeight(30)),
          Email(),
          SizedBox(height: getProportionateScreenHeight(30)),
          DateOfBirth(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPassword(),
          SizedBox(height: getProportionateScreenHeight(30)),
          ConfirPassword(),
          SizedBox(height: getProportionateScreenHeight(30)),
          DefaultButtonCustomeColor(
            color: kColorYellow,
            text: 'Register',
            press: () {
              prosesRegistrasi(
                  txtNamalengkap.text,
                  txtPhoneNomber.text,
                  txtUserName.text,
                  txtEmail.text,
                  txtDateBirthDay.text,
                  txtPassword.text,
                  txtConfirmPass.text);
            },
          ),
          SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, LoginScreen.routeName);
            },
            child: const Text(
              'Sudah punya akun? Masuk Disini',
              style: TextStyle(decoration: TextDecoration.underline),
            ),
          )
        ],
      ),
    );
  }

  TextFormField Namalengkap() {
    return TextFormField(
      controller: txtNamalengkap,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
        labelText: 'Nama Lengkap',
        hintText: 'Masukan Nama Lengkap ',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: const CustomSurffixIcon(svgIcon: 'assets/icons/User.svg'),
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return "Please enter Your name";
        }
        return null;
      },
    );
  }

  TextFormField PhoneNomber() {
    return TextFormField(
      controller: txtPhoneNomber,
      keyboardType: TextInputType.number,
      style: mTitleStyle,
      decoration: InputDecoration(
        labelText: 'Phone Number',
        hintText: 'Mobile Number ',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: const CustomSurffixIcon(svgIcon: 'assets/icons/Phone.svg'),
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return "Please enter Phone number";
        }
        if (value.length < 10) {
          return "Please enter Valid Phone number";
        }
        return null;
      },
    );
  }

  TextFormField DateOfBirth() {
    return TextFormField(
      controller: txtDateBirthDay,
      keyboardType: TextInputType.datetime,
      style: mTitleStyle,
      onTap: () async {
        DateTime? pickedDate = await showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(1950),
            //DateTime.now() - not to allow to choose before today.
            lastDate: DateTime(2100));

        if (pickedDate != null) {
          print(
              pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
          String formattedDate = DateFormat('yyyy-MM-dd').format(pickedDate);
          print(
              formattedDate); //formatted date output using intl package =>  2021-03-16
          setState(() {
            txtDateBirthDay.text =
                formattedDate; //set output date to TextField value.
          });
        } else {}
      },
      decoration: InputDecoration(
        labelText: 'Date Of Birth',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: const CustomSurffixIcon(svgIcon: 'assets/icons/watch.svg'),
      ),
    );
  }

  TextFormField buildUserName() {
    return TextFormField(
      controller: txtUserName,
      keyboardType: TextInputType.text,
      style: mTitleStyle,
      decoration: InputDecoration(
        labelText: 'Username',
        hintText: 'Masukan username anda',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: 'assets/icons/User.svg'),
      ),
    );
  }

  TextFormField Email() {
    return TextFormField(
      controller: txtEmail,
      keyboardType: TextInputType.emailAddress,
      style: mTitleStyle,
      decoration: InputDecoration(
        labelText: 'Email',
        hintText: 'Masukan Email Lengkap',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: 'assets/icons/Mail.svg'),
      ),
    );
  }

  TextFormField buildPassword() {
    return TextFormField(
      controller: txtPassword,
      obscureText: true,
      style: mTitleStyle,
      decoration: InputDecoration(
        labelText: 'Password',
        hintText: 'Masukan Password anda',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: 'assets/icons/Lock.svg'),
      ),
    );
  }

  TextFormField ConfirPassword() {
    return TextFormField(
      controller: txtConfirmPass,
      obscureText: true,
      style: mTitleStyle,
      decoration: InputDecoration(
        labelText: 'Password',
        hintText: 'Confirm Password',
        labelStyle: TextStyle(
            color: focusNode.hasFocus ? mSubtitleColor : kColorYellow),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomSurffixIcon(svgIcon: 'assets/icons/Lock.svg'),
      ),
    );
  }

  void prosesRegistrasi(
      String namaLengkap,
      String phoneNomor,
      String userName,
      String email,
      String dateOfBirth,
      String password,
      String confirmPass) async {
    try {
      // Buat objek data yang akan dikirim ke API
      final Map<String, dynamic> data = {
        'name': namaLengkap,
        'phone': phoneNomor,
        'username': userName,
        'email': email,
        'tanggal_lahir': dateOfBirth,
        'password': password,
        'confirm_password': confirmPass,
      };

      // Konversi data ke dalam format JSON
      final String jsonData = jsonEncode(data);

      // Kirim permintaan HTTP POST ke API
      final http.Response response = await http.post(
        Uri.parse(Constants.RegisUrl),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonData,
      );

      final responseData = json.decode(response.body);
      // menyesuaikan data postman
      final status = responseData['success'];
      final msg = responseData['message'];
      
      if (response.statusCode == 200) {
        if (status) {
          print('sukses');
          showSuccessDialog();
        } else {
          print('Gagal Registrasi: $msg');
          showFailureDialog('Gagal Registrasi: $msg');
        }
      } else {
        print('Gagal Registrasi: $msg');
        showFailureDialog('Gagal Registrasi: $msg');
      }
    } catch (error) {
      print('Error: $error');
      showServerErrorDialog('Error: $error');
    }
  }

  void showSuccessDialog() {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.success,
      animType: AnimType.rightSlide,
      desc: 'Berhasil Registrasi',
      btnOkOnPress: () {
        utilApps.hideDialog(context);
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => LoginScreen()));
      },
    ).show();
  }

  void showDataExistsDialog() {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.info,
      animType: AnimType.rightSlide,
      title: 'Informasi',
      desc: 'Data sudah Terdaftar',
      btnOkOnPress: () {
        utilApps.hideDialog(context);
      },
    ).show();
  }

  void showFailureDialog(String errorMessage) {
    // Tampilkan dialog gagal dengan pesan kesalahan
    // ignore: use_build_context_synchronously
    AwesomeDialog(
      context: context,
      dialogType: DialogType.error,
      animType: AnimType.rightSlide,
      title: 'Peringatan',
      desc: errorMessage,
      btnOkOnPress: () {
        utilApps.hideDialog(context);
      },
    ).show();
  }

  void showServerErrorDialog(String errorMessage) {
    // Tampilkan dialog kesalahan server
    // ignore: use_build_context_synchronously
    AwesomeDialog(
      context: context,
      dialogType: DialogType.error,
      animType: AnimType.rightSlide,
      title: 'Peringatan',
      desc: errorMessage,
      btnOkOnPress: () {
        utilApps.hideDialog(context);
      },
    ).show();
  }
}
