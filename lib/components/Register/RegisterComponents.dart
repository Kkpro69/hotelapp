import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../size_config.dart';
import 'Register_form.dart';

class RegisComponent extends StatefulWidget {
  const RegisComponent({super.key});

  @override
  _RegisComponent createState() => _RegisComponent();
}

class _RegisComponent extends State<RegisComponent> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenHeight(20),
                vertical: getProportionateScreenHeight(10)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.04,
                  ),
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.04,
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Join Out Membership',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontSize: 27,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      )),
                  SizedBox(
                    height: 20,
                  ),
                  RegisterForm()
                ],
              ),
            ),
          )),
    );
  }
}
