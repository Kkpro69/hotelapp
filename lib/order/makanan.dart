import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/API/models/Sliderbanner.dart';
import 'package:http/http.dart' as http;

import '../utils/constants.dart';

class MyFood extends StatefulWidget {
  const MyFood({
    super.key,
  });

  @override
  State<MyFood> createState() => _MyFoodState();
}

class _MyFoodState extends State<MyFood> {
  List<dynamic> Order = [];
  List<OrderMenu> Menus = [];

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(children: [
        SizedBox(
          child: Center(
            child: Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Container(
                    color: Colors.white70,
                    height: 400.0,
                    width: 300.0,
                  ),
                ),
                Positioned(
                  left: 10.0,
                  top: 20.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      width: 280,
                      child: Image.asset('assets/images/5.jpeg'),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 60.0,
                  left: 30.0,
                  child: Container(
                    child: Text(
                      'title',
                      style: GoogleFonts.ptSans(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 40.0,
                  left: 30.0,
                  child: Container(
                    child: Text(
                      '+90 Poin',
                      style: GoogleFonts.ptSans(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontSize: 15),
                    ),
                  ),
                ),
                Positioned(
                  right: 20,
                  bottom: 10,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      width: 100,
                      height: 40,
                      color: Colors.amber,
                      child: TextButton(
                        style: TextButton.styleFrom(
                          primary: Colors.black,
                        ),
                        onPressed: () {
                          ditekan();
                        },
                        child: Text(
                          'Rp.25.000',
                          style: GoogleFonts.ptSans(
                              fontWeight: FontWeight.normal, fontSize: 15),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}

class ItemMenu extends StatefulWidget {
  const ItemMenu({super.key});

  @override
  State<ItemMenu> createState() => _ItemMenuState();
}

class _ItemMenuState extends State<ItemMenu> {
  List<dynamic> Menus = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: Menus.length,
        itemBuilder: (context, index) {
          final menu = Menus[index];
          debugPrint('menu:$menu');
          final title = Menus[index].title;
          return ListTile(
            title: Text(title),
          //  title: Center(
          //     child: Column(children: [
          //       SizedBox(
          //         child: Center(
          //           child: Stack(
          //             children: <Widget>[
          //               ClipRRect(
          //                 borderRadius: BorderRadius.circular(12),
          //                 child: Container(
          //                   color: Colors.white70,
          //                   height: 400.0,
          //                   width: 300.0,
          //                 ),
          //               ),
          //               Positioned(
          //                 left: 10.0,
          //                 top: 20.0,
          //                 child: ClipRRect(
          //                   borderRadius: BorderRadius.circular(12),
          //                   child: Container(
          //                     width: 280,
          //                     child: Image.asset('assets/images/5.jpeg'),
          //                   ),
          //                 ),
          //               ),
          //               Positioned(
          //                 bottom: 60.0,
          //                 left: 30.0,
          //                 child: Container(
          //                   child: Text(
          //                     'title',
          //                     style: GoogleFonts.ptSans(
          //                         color: Colors.black,
          //                         fontWeight: FontWeight.bold,
          //                         fontSize: 20),
          //                   ),
          //                 ),
          //               ),
          //               Positioned(
          //                 bottom: 40.0,
          //                 left: 30.0,
          //                 child: Container(
          //                   child: Text(
          //                     '+90 Poin',
          //                     style: GoogleFonts.ptSans(
          //                         color: Colors.black,
          //                         fontWeight: FontWeight.normal,
          //                         fontSize: 15),
          //                   ),
          //                 ),
          //               ),
          //               Positioned(
          //                 right: 20,
          //                 bottom: 10,
          //                 child: ClipRRect(
          //                   borderRadius: BorderRadius.circular(12),
          //                   child: Container(
          //                     width: 100,
          //                     height: 40,
          //                     color: Colors.amber,
          //                     child: TextButton(
          //                       style: TextButton.styleFrom(
          //                         primary: Colors.black,
          //                       ),
          //                       onPressed: () {
          //                         ditekan();
          //                       },
          //                       child: Text(
          //                         'Rp.25.000',
          //                         style: GoogleFonts.ptSans(
          //                             fontWeight: FontWeight.normal, fontSize: 15),
          //                       ),
          //                     ),
          //                   ),
          //                 ),
          //               ),
          //             ],
          //           ),
          //         ),
          //       ),
          //     ]),
          //   )
          );
        },
      ),
    );
  }
}

void ditekan() async {
  print('ditekan');
  const url = Constants.OrderUrl;
  final response = await http.get(Uri.parse(url));
  final responseData = json.decode(response.body);
  final status = responseData['success'];

  try {
    if (response.statusCode == 200) {
      // print('Raw JSON Response: $body'); // Print the JSON data.

      // debugPrint("status $status");
      // debugPrint("JsonMap : $jsonMap");

      if (status) {
        print("API connected");
        print(responseData);
      } else {
        print('Invalid JSON structure');
      }
    } else {
      print('Failed to fetch data');
    }
  } catch (e) {
    print("error : $e");
  }
}
