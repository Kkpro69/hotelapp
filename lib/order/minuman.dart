import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyDrink extends StatefulWidget {
  const MyDrink({super.key});

  @override
  State<MyDrink> createState() => _MyDrinkState();
}

class _MyDrinkState extends State<MyDrink> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          SizedBox(
            child: Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 440.0,
                      width: 300.0,
                    ),
                  ),
                  Positioned(
                    left: 40.0,
                    top: 20.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 220,
                        child: Image.asset('assets/images/6.jpg'),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 90.0,
                    left: 30.0,
                    child: Container(
                      child: Text(
                        'Korean strawberry milk ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 70.0,
                    left: 30.0,
                    child: Container(
                      child: Text(
                        '+120 Poin',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 100,
                        height: 40,
                        color: Colors.amber,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Rp.35.000',
                            style: GoogleFonts.ptSans(
                                fontWeight: FontWeight.normal, fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            child: Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 440.0,
                      width: 300.0,
                    ),
                  ),
                  Positioned(
                    left: 40.0,
                    top: 20.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 220,
                        child: Image.asset('assets/images/6.jpg'),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 90.0,
                    left: 30.0,
                    child: Container(
                      child: Text(
                        'Korean strawberry milk ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 70.0,
                    left: 30.0,
                    child: Container(
                      child: Text(
                        '+120 Poin',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 100,
                        height: 40,
                        color: Colors.amber,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Rp.35.000',
                            style: GoogleFonts.ptSans(
                                fontWeight: FontWeight.normal, fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            child: Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 440.0,
                      width: 300.0,
                    ),
                  ),
                  Positioned(
                    left: 40.0,
                    top: 20.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 220,
                        child: Image.asset('assets/images/6.jpg'),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 90.0,
                    left: 30.0,
                    child: Container(
                      child: Text(
                        'Korean strawberry milk ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 70.0,
                    left: 30.0,
                    child: Container(
                      child: Text(
                        '+120 Poin',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 100,
                        height: 40,
                        color: Colors.amber,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Rp.35.000',
                            style: GoogleFonts.ptSans(
                                fontWeight: FontWeight.normal, fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            child: Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 440.0,
                      width: 300.0,
                    ),
                  ),
                  Positioned(
                    left: 40.0,
                    top: 20.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 220,
                        child: Image.asset('assets/images/6.jpg'),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 90.0,
                    left: 30.0,
                    child: Container(
                      child: Text(
                        'Korean strawberry milk ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 70.0,
                    left: 30.0,
                    child: Container(
                      child: Text(
                        '+120 Poin',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 100,
                        height: 40,
                        color: Colors.amber,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Rp.35.000',
                            style: GoogleFonts.ptSans(
                                fontWeight: FontWeight.normal, fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
