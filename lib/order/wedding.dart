import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/menu%20screen/deskripsi_wedding.dart';

class WeddingScreen extends StatelessWidget {
  const WeddingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          SizedBox(
            child: Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Color.fromARGB(255, 220, 220, 220),
                      height: 200.0,
                      width: 400.0,
                    ),
                  ),
                  Positioned(
                    top: 10.0,
                    left: 120.0,
                    child: Container(
                      child: Text(
                        'Local Event ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 25),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 50.0,
                    left: 10.0,
                    child: Container(
                      child: Text(
                        'Rp.15.000.000',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 80.0,
                    left: 10.0,
                    width: 300,
                    height: 150,
                    child: Stack(
                      children: [
                        Positioned(
                          child: Container(
                            child: Text('Termasuk :',
                                style: GoogleFonts.ptSans(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13)),
                          ),
                        ),
                        Positioned(
                          top: 20,
                          left: 10,
                          child: Container(
                            child: Text(
                                'Makeup + Hairdo/jilbab Akad/Pemberkatan/resepsi pengantin wanita',
                                style: GoogleFonts.ptSans(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 9)),
                          ),
                        ),
                        Positioned(
                          top: 40,
                          left: 10,
                          child: Container(
                            child: Text(
                                'Bunga segar (melati/mawar/baby breath) untuk pengantin',
                                style: GoogleFonts.ptSans(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 9)),
                          ),
                        ),
                        Positioned(
                          top: 60,
                          left: 10,
                          child: Container(
                            child: Text(
                                'Accesories pengantin termasuk kalung melati utk pengantin pria',
                                style: GoogleFonts.ptSans(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 9)),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 10,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 100,
                        height: 40,
                        color: Colors.amber,
                        child: Container(
                          alignment: Alignment.center,
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      const DeskripsiWedding(),
                                ),
                              );
                            },
                            child: Text(
                              'Lebih Lanjut',
                              style: GoogleFonts.ptSans(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 13),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.only(top: 10)),
          SizedBox(
            child: Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Color.fromARGB(255, 220, 220, 220),
                      height: 200.0,
                      width: 400.0,
                    ),
                  ),
                  Positioned(
                    top: 10.0,
                    left: 120.0,
                    child: Container(
                      child: Text(
                        'Local Event ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 25),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 50.0,
                    left: 10.0,
                    child: Container(
                      child: Text(
                        'Rp.15.000.000',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 80.0,
                    left: 10.0,
                    width: 300,
                    height: 150,
                    child: Stack(
                      children: [
                        Positioned(
                          child: Container(
                            child: Text('Termasuk :',
                                style: GoogleFonts.ptSans(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13)),
                          ),
                        ),
                        Positioned(
                          top: 20,
                          left: 10,
                          child: Container(
                            child: Text(
                                'Makeup + Hairdo/jilbab Akad/Pemberkatan/resepsi pengantin wanita',
                                style: GoogleFonts.ptSans(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 9)),
                          ),
                        ),
                        Positioned(
                          top: 40,
                          left: 10,
                          child: Container(
                            child: Text(
                                'Bunga segar (melati/mawar/baby breath) untuk pengantin',
                                style: GoogleFonts.ptSans(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 9)),
                          ),
                        ),
                        Positioned(
                          top: 60,
                          left: 10,
                          child: Container(
                            child: Text(
                                'Accesories pengantin termasuk kalung melati utk pengantin pria',
                                style: GoogleFonts.ptSans(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 9)),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 10,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 100,
                        height: 40,
                        color: Colors.amber,
                        child: Container(
                          alignment: Alignment.center,
                          child: GestureDetector(
                            onTap: () {
                              contoh();
                            },
                            child: Text(
                              'Lebih Lanjut',
                              style: GoogleFonts.ptSans(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 13),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class contoh extends StatefulWidget {
  const contoh({super.key});

  @override
  State<contoh> createState() => _contohState();
}

class _contohState extends State<contoh> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text('contoh '),
    );
  }
}
