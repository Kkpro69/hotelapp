import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/screen/home_screen.dart';

class MyVoucherScreen extends StatefulWidget {
  const MyVoucherScreen({super.key});

  @override
  State<MyVoucherScreen> createState() => _MyVoucherScreenState();
}

class _MyVoucherScreenState extends State<MyVoucherScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: BackButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen()));
          },
          color: Colors.black,
        ),
        backgroundColor: Colors.white70,
        elevation: 0,
        title: Text(
          'My Voucher ',
          style: GoogleFonts.ptSans(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.qr_code),
            onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                icon: Icon(
                  Icons.qr_code,
                  size: 150,
                ),
              ),
            ),
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {},
            color: Colors.black,
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 20),
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 160.0,
                      width: 400.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 30.0,
                    child: Container(
                      width: 200,
                      child: Image.asset('assets/images/4.jpeg'),
                    ),
                  ),
                  Positioned(
                    right: 50.0,
                    top: 30.0,
                    child: Container(
                      child: Text(
                        'Voucher Example',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 100.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Silver',
                        style: GoogleFonts.ptSans(
                            color: Colors.black, fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 90,
                        height: 40,
                        color: Colors.green[400],
                        child: TextButton(
                          style: TextButton.styleFrom(
                            alignment: Alignment.center,
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Bisa Di Claim',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontSize: 13,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 160.0,
                      width: 400.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 30.0,
                    child: Container(
                      width: 200,
                      child: Image.asset('assets/images/4.jpeg'),
                    ),
                  ),
                  Positioned(
                    right: 50.0,
                    top: 30.0,
                    child: Container(
                      child: Text(
                        'Voucher Example',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 100.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Silver',
                        style: GoogleFonts.ptSans(
                            color: Colors.black, fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 90,
                        height: 40,
                        color: Colors.green[400],
                        child: TextButton(
                          style: TextButton.styleFrom(
                            alignment: Alignment.center,
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Bisa Di Claim',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontSize: 13,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 160.0,
                      width: 400.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 30.0,
                    child: Container(
                      width: 200,
                      child: Image.asset('assets/images/4.jpeg'),
                    ),
                  ),
                  Positioned(
                    right: 50.0,
                    top: 30.0,
                    child: Container(
                      child: Text(
                        'Voucher Example',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 100.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Silver',
                        style: GoogleFonts.ptSans(
                            color: Colors.black, fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 90,
                        height: 40,
                        color: Colors.green[400],
                        child: TextButton(
                          style: TextButton.styleFrom(
                            alignment: Alignment.center,
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Bisa Di Claim',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontSize: 13,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 160.0,
                      width: 400.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 30.0,
                    child: Container(
                      width: 200,
                      child: Image.asset('assets/images/4.jpeg'),
                    ),
                  ),
                  Positioned(
                    right: 50.0,
                    top: 30.0,
                    child: Container(
                      child: Text(
                        'Voucher Example',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 100.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Silver',
                        style: GoogleFonts.ptSans(
                            color: Colors.black, fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 90,
                        height: 40,
                        color: Colors.green[400],
                        child: TextButton(
                          style: TextButton.styleFrom(
                            alignment: Alignment.center,
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Bisa Di Claim',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontSize: 13,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.white70,
                      height: 160.0,
                      width: 400.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 30.0,
                    child: Container(
                      width: 200,
                      child: Image.asset('assets/images/4.jpeg'),
                    ),
                  ),
                  Positioned(
                    right: 50.0,
                    top: 30.0,
                    child: Container(
                      child: Text(
                        'Voucher Example',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 100.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Silver',
                        style: GoogleFonts.ptSans(
                            color: Colors.black, fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20,
                    bottom: 10,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        width: 90,
                        height: 40,
                        color: Colors.green[400],
                        child: TextButton(
                          style: TextButton.styleFrom(
                            alignment: Alignment.center,
                            primary: Colors.black,
                          ),
                          onPressed: () {},
                          child: Text(
                            'Bisa Di Claim',
                            style: GoogleFonts.ptSans(
                                color: Colors.black,
                                fontSize: 13,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
