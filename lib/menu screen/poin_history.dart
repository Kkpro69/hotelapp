import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/screen/home_screen.dart';

class PoinHistory extends StatefulWidget {
  const PoinHistory({super.key});

  @override
  State<PoinHistory> createState() => _PoinHistoryState();
}

class _PoinHistoryState extends State<PoinHistory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: BackButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen()));
          },
          color: Colors.black,
        ),
        backgroundColor: Colors.white70,
        elevation: 0,
        title: Text(
          'Poin History ',
          style: GoogleFonts.ptSans(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.qr_code),
            onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                icon: Icon(
                  Icons.qr_code,
                  size: 150,
                ),
              ),
            ),
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {},
            color: Colors.black,
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 30),
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12),
                          borderRadius: BorderRadius.circular(12)),
                      height: 120.0,
                      width: 380.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '11 Agu 2023',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 130.0,
                    top: 10.0,
                    child: Container(
                      height: 20,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    left: 150.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '18733',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 45.0,
                    child: Container(
                      child: Text(
                        'Hadi ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 70.0,
                    child: Container(
                      child: Text(
                        'Note',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20.0,
                    bottom: 20.0,
                    child: Container(
                      child: Text(
                        'Poin Bertambah : 90',
                        style: GoogleFonts.ptSans(
                            color: Colors.green,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 130.0,
                    top: 50.0,
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    right: 27.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Rp.12.000.000',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 15)),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12),
                          borderRadius: BorderRadius.circular(12)),
                      height: 120.0,
                      width: 380.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '11 Agu 2023',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 130.0,
                    top: 10.0,
                    child: Container(
                      height: 20,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    left: 150.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '18733',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 45.0,
                    child: Container(
                      child: Text(
                        'Hadi ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 70.0,
                    child: Container(
                      child: Text(
                        'Note',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 20.0,
                    bottom: 20.0,
                    child: Container(
                      child: Text(
                        'Poin Berkurang : 90',
                        style: GoogleFonts.ptSans(
                            color: Colors.red,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 130.0,
                    top: 50.0,
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    right: 27.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Rp.12.000.000',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
