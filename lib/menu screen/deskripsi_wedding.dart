import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'more_screen.dart';

class DeskripsiWedding extends StatelessWidget {

  static String routeName = "/Deskripsi_wedding";
  const DeskripsiWedding({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: BackButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => MoreScreen()));
          },
          color: Colors.black,
        ),
        backgroundColor: Colors.white70,
        elevation: 0,
        title: Text(
          'Deskripsi',
          style: GoogleFonts.ptSans(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.qr_code),
            onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                icon: Icon(
                  Icons.qr_code,
                  size: 150,
                ),
              ),
            ),
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {},
            color: Colors.black,
          ),
        ],
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, top: 10),
            width: 400,
            color: Colors.white70,
            child: Text('MakeUp :',
                style: GoogleFonts.ptSans(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
          ),
          Container(
            padding: EdgeInsets.only(left: 14, top: 3),
            width: 400,
            child: Text(
                'Makeup + Hairdo/jilbab Akad/Pemberkatan/resepsi pengantin wanita',
                style: GoogleFonts.ptSans(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 15)),
          ),
          Container(
            padding: EdgeInsets.only(left: 14, top: 3),
            width: 400,
            child: Text(
                'Bunga segar (melati/mawar/baby breath) untuk pengantin',
                style: GoogleFonts.ptSans(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 15)),
          ),
          Container(
            padding: EdgeInsets.only(left: 14, top: 3),
            width: 400,
            child: Text(
                'Accesories pengantin termasuk kalung melati utk pengantin pria',
                style: GoogleFonts.ptSans(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 15)),
          ),
        ],
      ),
    );
  }
}
