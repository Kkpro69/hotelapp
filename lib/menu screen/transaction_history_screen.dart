import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/screen/home_screen.dart';

class TransactionHistory extends StatefulWidget {
  const TransactionHistory({super.key});

  @override
  State<TransactionHistory> createState() => _TransactionHistoryState();
}

class _TransactionHistoryState extends State<TransactionHistory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: BackButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen()));
          },
          color: Colors.black,
        ),
        backgroundColor: Colors.white70,
        elevation: 0,
        title: const Text(
          'Transaction History',
          style: TextStyle(color: Colors.black),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.qr_code),
            onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                icon: Icon(
                  Icons.qr_code,
                  size: 150,
                ),
              ),
            ),
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {},
            color: Colors.black,
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 30),
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12),
                          borderRadius: BorderRadius.circular(12)),
                      height: 120.0,
                      width: 380.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '11 Agu 2023',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 130.0,
                    top: 10.0,
                    child: Container(
                      height: 20,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    left: 150.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '18733',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 45.0,
                    child: Container(
                      child: Text(
                        'Hadi ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 70.0,
                    child: Container(
                      child: Text(
                        'Category ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 50.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Total Transaksi',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 130.0,
                    top: 40.0,
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    right: 20.0,
                    top: 65.0,
                    child: Container(
                      child: Text(
                        'Rp.12.000.000',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 15)),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12),
                          borderRadius: BorderRadius.circular(12)),
                      height: 120.0,
                      width: 380.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '11 Agu 2023',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 130.0,
                    top: 10.0,
                    child: Container(
                      height: 20,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    left: 150.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '18733',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 45.0,
                    child: Container(
                      child: Text(
                        'Hadi ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 70.0,
                    child: Container(
                      child: Text(
                        'Category ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 50.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Total Transaksi',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 130.0,
                    top: 40.0,
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    right: 20.0,
                    top: 65.0,
                    child: Container(
                      child: Text(
                        'Rp.12.000.000',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 15)),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black12),
                          borderRadius: BorderRadius.circular(12)),
                      height: 120.0,
                      width: 380.0,
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '11 Agu 2023',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 130.0,
                    top: 10.0,
                    child: Container(
                      height: 20,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    left: 150.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        '18733',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 45.0,
                    child: Container(
                      child: Text(
                        'Hadi ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 20.0,
                    top: 70.0,
                    child: Container(
                      child: Text(
                        'Category ',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 50.0,
                    top: 50.0,
                    child: Container(
                      child: Text(
                        'Total Transaksi',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 130.0,
                    top: 40.0,
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color.fromARGB(37, 103, 101, 101)))),
                    ),
                  ),
                  Positioned(
                    right: 20.0,
                    top: 65.0,
                    child: Container(
                      child: Text(
                        'Rp.12.000.000',
                        style: GoogleFonts.ptSans(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
