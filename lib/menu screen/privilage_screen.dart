import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/screen/home_screen.dart';

class PrivilageScreen extends StatefulWidget {
  const PrivilageScreen({super.key});

  @override
  State<PrivilageScreen> createState() => _PrivilageScreenState();
}

class _PrivilageScreenState extends State<PrivilageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: BackButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen()));
          },
          color: Colors.black,
        ),
        backgroundColor: Colors.white70,
        elevation: 0,
        title: Text(
          'Privilage',
          style: GoogleFonts.ptSans(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 35),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.qr_code),
            onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                icon: Icon(
                  Icons.qr_code,
                  size: 150,
                ),
              ),
            ),
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {},
            color: Colors.black,
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 20),
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.black87,
                      height: 210.0,
                      width: 380.0,
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        'SILVER',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 40),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    top: 100.0,
                    child: Container(
                      child: Text(
                        'Froton Hotel Sragen',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    bottom: 40.0,
                    child: Container(
                      child: Text(
                        'Total Poin',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    bottom: 10.0,
                    child: Container(
                      child: Text(
                        '0',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    top: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12),
                            bottomRight: Radius.circular(12)),
                        color: Color.fromARGB(192, 192, 192, 192),
                      ),
                      width: 95,
                      height: 210.0,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.black87,
                      height: 210.0,
                      width: 380.0,
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        'GOLD',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 40),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    top: 100.0,
                    child: Container(
                      child: Text(
                        'Froton Hotel Sragen',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    bottom: 40.0,
                    child: Container(
                      child: Text(
                        'Total Poin',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    bottom: 10.0,
                    child: Container(
                      child: Text(
                        '1000',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    top: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12),
                            bottomRight: Radius.circular(12)),
                        color: Color.fromARGB(192, 224, 201, 47),
                      ),
                      width: 95,
                      height: 210.0,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Container(
                      color: Colors.black87,
                      height: 210.0,
                      width: 380.0,
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    top: 10.0,
                    child: Container(
                      child: Text(
                        'PLATINUM',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 40),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    top: 100.0,
                    child: Container(
                      child: Text(
                        'Froton Hotel Sragen',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    bottom: 40.0,
                    child: Container(
                      child: Text(
                        'Total Poin',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30.0,
                    bottom: 10.0,
                    child: Container(
                      child: Text(
                        '12000',
                        style: GoogleFonts.ptSans(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 20),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    top: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(12),
                            bottomRight: Radius.circular(12)),
                        color: Color.fromARGB(170, 132, 131, 131),
                      ),
                      width: 95,
                      height: 210.0,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
