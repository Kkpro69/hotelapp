import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/order/makanan.dart';
import 'package:hotel_app_2/order/minuman.dart';
import 'package:hotel_app_2/screen/home_screen.dart';

import '../API/api/api.dart';
import '../API/models/Sliderbanner.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({super.key});
  
  @override
  State<OrderScreen> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  late Future<List<OrderMenu>> menuInOrder;

  @override
  void initState() {
    super.initState();
    menuInOrder = Api().getToApiOrder();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: BackButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen()));
          },
          color: Colors.black,
        ),
        backgroundColor: Colors.white70,
        elevation: 0,
        title: Text(
          'Order',
          style: GoogleFonts.ptSans(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 25,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.qr_code),
            onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                icon: Icon(
                  Icons.qr_code,
                  size: 150,
                ),
              ),
            ),
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {},
            color: Colors.black,
          ),
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {},
            color: Colors.black,
          ),
        ],
      ),
      body: SizedBox(
        child: FutureBuilder(
          future: menuInOrder,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error.toString()),
              );
            } else if (snapshot.hasData) {
              final data = snapshot.data;
              return pesan(
                snapshot: snapshot,
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}

class pesan extends StatelessWidget {
  const pesan({
    super.key,
    required this.snapshot,
  });
  final AsyncSnapshot snapshot;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: snapshot.data!.length,
        child: Scaffold(
          appBar: TabBar(
            indicatorColor: Colors.black,
            tabs: [
              Tab(
                child: CarouselSlider.builder(
                  itemCount: snapshot.data!.length,
                  options: CarouselOptions(
                    autoPlay: false,
                  ),
                  itemBuilder: (context, itemIndex, pageViewIndex) {
                    return GestureDetector(
                      onTap: () {},
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: SizedBox(
                          height: 200,
                          child: Text(
                            (snapshot.data[itemIndex]).name,
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
          body: const TabBarView(
            children: [
              SingleChildScrollView(
                child: ItemMenu(),
              ),
              SingleChildScrollView(
                child: MyDrink(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
