import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_app_2/order/wedding.dart';
import 'package:hotel_app_2/screen/home_screen.dart';

class MoreScreen extends StatelessWidget {
  const MoreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: null,
      body: const tabs(),
    );
  }
}

class tabs extends StatefulWidget {
  const tabs({super.key});

  @override
  State<tabs> createState() => _tabsState();
}

class _tabsState extends State<tabs> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            leadingWidth: 50,
            leading: BackButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (BuildContext context) => HomeScreen()));
              },
              color: Colors.black,
            ),
            backgroundColor: Colors.white70,
            elevation: 0,
            title: Text(
              'More',
              style: GoogleFonts.ptSans(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 25),
            ),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.qr_code),
                onPressed: () => showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    icon: Icon(
                      Icons.qr_code,
                      size: 150,
                    ),
                  ),
                ),
                color: Colors.black,
              ),
              IconButton(
                icon: const Icon(Icons.notifications),
                onPressed: () {},
                color: Colors.black,
              ),
              IconButton(
                icon: const Icon(Icons.person),
                onPressed: () {},
                color: Colors.black,
              ),
            ],
            bottom: TabBar(
              indicatorColor: Colors.black,
              tabs: [
                Tab(
                  child: Text(
                    'Wedding',
                    style: GoogleFonts.ptSans(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                        fontSize: 15),
                  ),
                ),
                Tab(
                  child: Text(
                    'Event',
                    style: GoogleFonts.ptSans(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                        fontSize: 15),
                  ),
                ),
                Tab(
                  child: Text(
                    'Arisan',
                    style: GoogleFonts.ptSans(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                        fontSize: 15),
                  ),
                ),
              ],
            ),
          ),
          body: const TabBarView(
            children: [
              SingleChildScrollView(
                padding: EdgeInsets.only(top: 30),
                child: WeddingScreen(),
              ),
              Center(child: Text('Event')),
              Center(child: Text('Arisan')),
            ],
          ),
        ),
      ),
    );
  }
}
